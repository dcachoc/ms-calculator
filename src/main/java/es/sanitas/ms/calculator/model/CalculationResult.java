package es.sanitas.ms.calculator.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CalculationResult implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int result;
	
}
