package es.sanitas.ms.calculator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import es.sanitas.ms.calculator.util.Operation;
import es.sanitas.ms.calculator.util.OperationAdd;
import es.sanitas.ms.calculator.util.OperationSubtract;
import io.corp.calculator.TracerImpl;

@Configuration
@ComponentScan("es.sanitas.ms.calculator")
public class MsCalculatorConfiguration {

	@Bean
	public TracerImpl getTracerImpl() { // Se devuelve como la implementación en lugar de la interfaz ya que la clase
										// TracerImpl no implementa TracerAPI
		return new TracerImpl();
	}

	@Bean(name = "operationAdd")
	public Operation getOperationAdd() {
		return new OperationAdd();
	}

	@Bean(name = "operationSubtract")
	public Operation getOperationSubtract() {
		return new OperationSubtract();
	}
}
