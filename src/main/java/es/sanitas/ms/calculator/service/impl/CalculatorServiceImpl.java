package es.sanitas.ms.calculator.service.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import es.sanitas.ms.calculator.model.CalculationResult;
import es.sanitas.ms.calculator.service.CalculatorService;
import es.sanitas.ms.calculator.util.Operation;
import io.corp.calculator.TracerImpl;

@Service
public class CalculatorServiceImpl implements CalculatorService {

	@Autowired
	private TracerImpl tracer;
	
	@Autowired
	@Qualifier("operationAdd")
	private Operation operationAdd;
	
	@Autowired
	@Qualifier("operationSubtract")
	private Operation operationSubtract;
	
	@Override
	public CalculationResult add(Collection<Integer> values) {
		CalculationResult result = this.operation(values, operationAdd);
		tracer.trace(result.getResult());
		return result;
	}
	
	@Override
	public CalculationResult subtract(Collection<Integer> values) {
		CalculationResult result = this.operation(values, operationSubtract);
		tracer.trace(result.getResult());
		return result;
	}
	
	@Override
	public CalculationResult operation(Collection<Integer> values, Operation operation) {
		Integer result = null;
		for(Integer value: values) {
			result = operation.operate(result, value);
		}
		return CalculationResult.builder().result(result).build();
	}
}
