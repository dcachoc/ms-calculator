package es.sanitas.ms.calculator.service;

import java.util.Collection;

import es.sanitas.ms.calculator.model.CalculationResult;
import es.sanitas.ms.calculator.util.Operation;

public interface CalculatorService {

	public CalculationResult add(Collection<Integer> values);
	
	public CalculationResult subtract(Collection<Integer> values);
	
	public CalculationResult operation(Collection<Integer> values, Operation operation);
	
}
