package es.sanitas.ms.calculator.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import es.sanitas.ms.calculator.model.CalculationResult;
import es.sanitas.ms.calculator.service.impl.CalculatorServiceImpl;

@RestController
public class CalculatorREST {

	@Autowired
	private CalculatorServiceImpl calculatorService;
	
	@GetMapping("/add/{op1}/{op2}")
	public CalculationResult add(@PathVariable int op1, @PathVariable int op2) {
		return calculatorService.add(Arrays.asList(op1, op2));
	}

	@GetMapping("/subtract/{op1}/{op2}")
	public CalculationResult subtract(@PathVariable int op1, @PathVariable int op2) {
		return calculatorService.subtract(Arrays.asList(op1, op2));
	}
}
