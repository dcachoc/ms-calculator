package es.sanitas.ms.calculator.util;

public class OperationSubtract extends Operation {

	@Override
	public Integer operate(Integer op1, Integer op2) {
		if (op1 != null) {
			return op1 - op2;
		} else {
			return op2;
		}
	}

}
