package es.sanitas.ms.calculator.util;

public abstract class Operation {

	protected Integer defaultResult = 0;
	
	public abstract Integer operate(Integer op1, Integer op2);
	
}
