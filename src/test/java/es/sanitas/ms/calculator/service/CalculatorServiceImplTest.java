package es.sanitas.ms.calculator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import es.sanitas.ms.calculator.service.impl.CalculatorServiceImpl;

@SpringBootTest
public class CalculatorServiceImplTest {
	
	@Autowired
	private CalculatorServiceImpl calculatorService;
	
	@Test
    void add() {
		assertEquals(calculatorService.add(Arrays.asList(1, 2)).getResult(), 3);
    }
	
	@Test
    void subtract() {		
		assertEquals(calculatorService.subtract(Arrays.asList(1, 2)).getResult(), -1);
    }
}
