package es.sanitas.ms.calculator.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import es.sanitas.ms.calculator.model.CalculationResult;
import es.sanitas.ms.calculator.service.impl.CalculatorServiceImpl;

@SpringBootTest
@AutoConfigureMockMvc
public class CalculatorRESTTest {

	@MockBean
	private CalculatorServiceImpl calculatorService;

	@Autowired
	private MockMvc mvc;

	@InjectMocks
	private CalculatorREST calculatorREST;

	@Test
	public void addTest() throws Exception {
		CalculationResult mockResult = CalculationResult.builder().result(3).build();
		when(calculatorService.add(ArgumentMatchers.anyCollection())).thenReturn(mockResult);

		mvc.perform(MockMvcRequestBuilders.get("/add/1/2").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.result").exists())
				.andExpect(jsonPath("$.result", is(3)));

		verify(calculatorService, times(1)).add(Arrays.asList(1, 2));
	}

	@Test
	public void subtractTest() throws Exception {
		CalculationResult mockResult = CalculationResult.builder().result(-1).build();
		when(calculatorService.subtract(ArgumentMatchers.anyCollection())).thenReturn(mockResult);

		mvc.perform(MockMvcRequestBuilders.get("/subtract/1/2").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.result").exists())
				.andExpect(jsonPath("$.result", is(-1)));

		verify(calculatorService, times(1)).subtract(Arrays.asList(1, 2));
	}
}
